import Vue from 'vue'
import Router from 'vue-router'
import Contact from '@/components/pages/Contact'
import About from '@/components/pages/About'
import TaskList from '@/components/pages/task/List'
import TaskNew from '@/components/pages/task/New'
import TaskId from '@/components/pages/task/_id'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      redirect: '/task/list'
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/task/list',
      name: 'TaskList',
      component: TaskList
    },
    {
      path: '/task/new',
      name: 'TaskNew',
      component: TaskNew
    },
    {
      path: '/task/:id',
      name: 'TaskId',
      component: TaskId
    }
  ]
})
